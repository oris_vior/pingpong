

public class Main {
    public static void main(String[] args) throws InterruptedException {
        PingPongGame game = new PingPongGame();
        game.startGame();
    }
}


class PingPongThread extends Thread {
    PingPongThread(String name) {
        this.setName(name);
    }

    @Override
    public void run() {
        Ball ball = Ball.getBall();
        while (ball.isInGame()) {
            kickBall(ball);
        }
    }

    private void kickBall(Ball ball) {
        if (!ball.getSide().equals(getName())) {
            ball.kick(getName());
        }
    }
}


class Ball {
    private static final Ball instance = new Ball();
    private int kicks = 0;
    private String side = "";

    private Ball() {
    }

    static Ball getBall() {
        return instance;
    }

    synchronized void kick(String playername) {
        kicks++;
        side = playername;
        System.out.println(kicks + " " + side);
    }

    String getSide() {
        return side;
    }

    boolean isInGame() {
        return (kicks < 15);
    }
}


class PingPongGame {
    PingPongThread player1 = new PingPongThread("Ping");
    PingPongThread player2 = new PingPongThread("Pong");

    Ball ball;

    PingPongGame() {
        ball = Ball.getBall();
    }

    void startGame() throws InterruptedException {
        player1.start();
        player2.start();
    }
}